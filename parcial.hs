import Data.List
import qualified Data.Map as Map

main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solve . parselines . tail . lines

output :: Int -> [Int] -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ s res

s :: [Int] -> String
s = unwords . map show

parselines :: [String] -> [[Int]]
parselines (n:ls) = devThis : parselines devThat
      where devThis = map read  $ words . unlines $ take ((2*read n)-1) ls
            devThat = drop ((2*read n)-1) ls
parselines _ = []

solve :: [[Int]] -> [[Int]]
solve = map (sort . map fst . filter m . foldl' count' [])

m :: (Int, Int) -> Bool
m (_ ,b) =  b `mod` 2 /= 0

count' :: [(Int, Int)] -> Int -> [(Int, Int)]
count' [] b = [(b, 1)]
count' (e@(a, b) : c) d
      | a == d = (a, b + 1) : c
      | otherwise = e : count' c d
