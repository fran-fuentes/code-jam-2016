import Data.List
import qualified Data.Map.Strict as Map

main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solve . parselines . tail . lines

output :: Int -> [Int] -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ s res

s :: [Int] -> String
s = unwords . map show

parselines :: [String] -> [[Int]]
parselines (n:ls) = devThis : parselines devThat
      where devThis = map read  $ words . unlines $ take ((2*read n)-1) ls
            devThat = drop ((2*read n)-1) ls
parselines _ = []

solve :: [[Int]] -> [[Int]]
solve = map (Map.keys . Map.filter m . foldl' count' Map.empty)

m ::  Int -> Bool
m b =  b `mod` 2 /= 0

count' :: Map.Map Int Int -> Int -> Map.Map Int Int
count' m v = Map.alter l v m
    where l Nothing = Just 1
          l (Just b) = Just (b+1)
