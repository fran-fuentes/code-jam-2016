-- Baa cordero oveja leal a tu vellón
import Data.List

main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solveAll . tail . lines

output :: Int -> String -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ res

solveAll :: [String] -> [String]
solveAll = map solveOne

-- solveOne: resolve only one number and returns the string
solveOne :: String -> String
solveOne [] = []
solveOne (a:rest) = foldl' pushOn [a] rest

pushOn :: String -> Char -> String
pushOn [] a = [a]
pushOn m@(b:_) c
  | c >= b = c : m
  | otherwise = m ++ [c]
