import Control.Monad
import Data.List


main = do
  putStrLn "Case #1:"
  let divs = [k^8 + 1 | k <- [2..10]]
  let lst = take 50 (mapM (const "01") [1..6])
  mapM_ (\x -> putStrLn $ "1" ++ x ++ "11" ++ x ++ "1 " ++ intercalate " " (map show divs)) lst
