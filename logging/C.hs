import Data.List

main :: IO ()
main = interact $
  concat . zipWith output [1..] . solveAll . tail . lines

output :: Int -> [Int] -> String
output tc res = "Case #" ++ show tc ++ ":\n" ++ unlines (map show res)

solveAll :: [String] -> [[Int]]
solveAll [] = []
solveAll (line1:ls) = solve (map parsePoint ls1) : solveAll ls2
    where (ls1,ls2) = splitAt n ls
          n = read line1

data Point = Point Int Int deriving Eq

parsePoint :: String -> Point
parsePoint line = Point x y where [x,y] = map read (words line)

sub :: Point -> Point -> Point
sub (Point ax ay) (Point bx by) = Point (ax-bx) (ay-by)

cross :: Point -> Point -> Int
cross (Point ax ay) (Point bx by) = ax * by - ay * bx

cmpPoint :: Point -> Point -> Ordering
cmpPoint a b
    | sa < sb  = LT
    | sa > sb  = GT
    | otherwise = compare 0 (cross a b)
    where sa = side a
          sb = side b

side :: Point -> Int
side (Point x y)
    | y < 0  = 0
    | y > 0  = 1
    | x < 0  = 0
    | otherwise = 1

solve :: [Point] -> [Int]
solve points = map solve1 (extracts points)

extracts :: [a] -> [(a,[a])]
extracts = extracts' []

extracts' :: [a] -> [a] -> [(a,[a])]
extracts' _ [] = []
extracts' ac (x:xs) = (x, ac ++ xs) : extracts' (x:ac) xs

solve1 :: (Point, [Point]) -> Int
solve1 (_, []) = 0
solve1 (center, points) = minimum $ solve2 0 vectors (cycle vectors)
    where vectors = map (\ps -> (head ps, length ps)) $
                    groupBy (\a b -> cmpPoint a b == EQ) $
                    sortBy cmpPoint [sub p center | p <- points]

solve2 :: Int -> [(Point,Int)] -> [(Point,Int)] -> [Int]
solve2 _ [] _ = []
solve2 skip points@((p,cntp):points') cyc@((c,cntc):cyc')
  | skip == 0  = doSkip
  | angle > 0  = doSkip
  | otherwise = enoughSkipped
  where doSkip = solve2 (skip+cntc) points cyc'
        enoughSkipped = (skip-cntp) : solve2 (skip-cntp) points' cyc
        angle = cross p c
