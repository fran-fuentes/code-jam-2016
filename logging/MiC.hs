import Data.List.Split

main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solveAll . tail . lines

output :: Int -> [Int] -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ unwords (fmap show res)

solveAll :: [String] -> [[Int]]
solveAll [] = []
solveAll ( _:line2:ls) = solve lines2 : solveAll ls
    where lines2 = map read (splitOn " " line2)

solve :: [Int] -> [Int]
solve [] = []
solve list = [modo1, modo2]
  -- the n parameter is useless.
  -- first, for every difference, sum its
  -- second...hum...
  where listt = zip list (tail list)
        modo1 = sum $ map solveSmall listt
        m = maximum $ map abs (map (-) listt)
        modo2 = sum $ map (solveBig m) listt

solveSmall :: (Int, Int) -> Int
solveSmall (a, b)
        | b > a = 0
        | b <= a = a -b

solveBig :: Int -> (Int, Int) -> Int
solveBig a (b, c)
      | b > a = a
      |otherwise = b
