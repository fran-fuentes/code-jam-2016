main :: IO ()
main = interact $
  unlines . zipWith output [1..] . map solve . parselines . tail . lines

output :: Int -> Int -> String
output tc answer = "Case #" ++ show tc ++ ": " ++ show answer

parselines :: [String] -> [[Int]]
parselines (_:line:ls) = map read (words line) : parselines ls
parselines _ = []

solve :: [Int] -> Int
solve xs = minimum [d + splits d xs | d <- [1..m]] where m = maximum xs

splits :: Int -> [Int] -> Int
splits d xs = sum [(x-1) `div` d | x <- xs]
