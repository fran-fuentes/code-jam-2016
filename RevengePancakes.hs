
main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solveAll . tail . lines

output :: Int -> Int -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ show res

solveAll :: [String] -> [Int]
solveAll [] = []
solveAll (a:resto) = solveOne a : solveAll resto

solveOne :: String -> Int
solveOne "" = 0
solveOne m@(a:rest) = o
  where h = foldl counting (a,0) m
        n = snd h
        o
          | fst h == '-' = n +1
          | otherwise = n

counting :: (Char, Int) -> Char -> (Char, Int)
counting ('+', a) '-' = ('-', a + 1)
counting ('-', a) '+' = ('+', a + 1)
counting (c, b) _ = (c, b)
