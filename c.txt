main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solve . tail . lines

output :: Int -> Bool -> String
output tc answer = "Case #" ++ show tc ++ ": " ++ myshow answer

myshow :: Bool -> String
myshow False = "NO"
myshow True = "YES"

solve :: [String] -> [Bool]
solve (line1:line2:ls) = solve' str : solve ls where
  x = (read $ words line1 !! 1) :: Int
  rx = if x>=12 then 12 + x `mod` 4 else x
  str = concat $ replicate rx line2
solve [] = []
solve [_] = undefined

data HQuat = One | I | J | K deriving Eq
data Quat = Negative HQuat | Positive HQuat deriving Eq

hmul :: HQuat -> HQuat -> Quat
hmul One x = Positive x
hmul x One = Positive x
hmul I I = Negative One
hmul I J = Positive K
hmul I K = Negative J
hmul J I = Negative K
hmul J J = Negative One
hmul J K = Positive I
hmul K I = Positive J
hmul K J = Negative I
hmul K K = Negative One

neg :: Quat -> Quat
neg (Negative x) = Positive x
neg (Positive x) = Negative x

mul :: Quat -> HQuat -> Quat
mul (Negative a) b = neg (hmul a b)
mul (Positive a) b = hmul a b

solve' :: String -> Bool
solve' s = containsSubsequence [Positive I, Positive K] xs && last xs == Negative One
          where xs = scanl mul (Positive One) (map charToHQuat s)

charToHQuat :: Char -> HQuat
charToHQuat 'i' = I
charToHQuat 'j' = J
charToHQuat 'k' = K
charToHQuat _ = undefined

containsSubsequence :: Eq a => [a] -> [a] -> Bool
containsSubsequence [] _ = True
containsSubsequence _ [] = False
containsSubsequence xs@(x:xt) (y:yt)
  | x == y    = containsSubsequence xt yt
  | otherwise = containsSubsequence xs yt
