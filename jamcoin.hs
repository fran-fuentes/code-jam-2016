{-# LANGUAGE ScopedTypeVariables #-}
import Control.Monad
import Control.Applicative
import Data.List

factor :: Integer -> Maybe Integer
factor x =
  case filter (\d -> x `mod` d == 0) [2..min (x-1) 100] of
  (x : _) -> Just x
  [] -> Nothing

mine l =
  mapM (\base -> factor (foldl' (\a x -> base * a + x) 0 l)) [2..10]

solve n j = take j $
  fmap (\l -> [1] ++ l ++ [1]) (replicateM (n - 2) [0,1])
  >>= \s -> case mine s of
   Nothing -> []
   Just divisors -> [(s, divisors)]

format = unlines . map (\(s, divisors) -> unwords $ (s >>= show) : map show divisors)

solveInput = do
  [n, (j :: Int)] <- map read . words <$> getLine
  return $ format $ solve n j


main = do
  n <- readLn
  flip mapM_ [1..n] $ \tc -> do
    s <- solveInput
    putStrLn $ "Case #" ++ show tc ++ ":\n" ++ s
