-- Baa-ram-ewe, Baa-ram-ewe. To your breed, your fleece, your clan be True
-- Baa-ram-ewe
---     Babe
import Data.Char
import Data.List

main :: IO ()
main = interact $
  unlines . zipWith output [1..] . parselines . tail . lines

output :: Int -> [Int] -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ s res

s :: [Int] -> String
s = unlines . map show

solve :: [Int] -> String
solve a = map fst $ filter t $ foldl' count [] a
    where t x = snd x mod 2 /= 0

-- para la lista de todos los problemas
parselines :: [String] -> [[Int]]
parselines (n:ls) = devThis : parselines devThat
      where devThis = map read  $ words . unlines $ take ((2*read n)+1) ls
            devThat = drop ((2*read n)+1) ls
parselines _ = []

count :: [(Int, Int)] -> Int -> [(Int, Int)]
count [] b = [(b, 1)]
count (e@(a, b) : c) d
      | a == d = (a, b + 1) : c
      | otherwise = e : count c d
